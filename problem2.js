const fs = require('fs');
const path = require('path');

function readFile(filePath) {
    // body...
    fs.readFile(filePath, 'utf-8', (err, data) => {
        if(err){
            console.log(err);
        }
        else{
            console.log('Read the file');
        }
    });
}

function readFileUpper(data) {
    // body...
    upper = data.toString().toUpperCase();
    return upper;

}

function readFileLower(data) {
    // body...
    lower = data.toString().toLowerCase();
    return lower;
}

function writeFile(filePath, content){
    fs.writeFile(filePath, 'utf-8', (err) =>{
        if(err){
            console.log(err);
        }else{
            console.log(content);
        }
    })
}


function appendFile(filePath, data){
    fs.appendFile(filePath, data, (err) => {
        if(err){
            console.log(err);
        }else{
            console.log(data);
        }
    })
}


function deleteFile(filePath) {
    fs.unlink(filePath, 'utf-8', (err) => {
        if(err){
            console.log(err);
        }else{
            console.log(`Deleted ${filePath}`);
        }
    })

}
// split into sentences
const splitBySentence = (data) => {
  return data.replace(/([.?!])(\s)*(?=[A-Z])/g, "$1|")
    .split("|")
    .filter(sentence => !!sentence)
    .map(sentence => sentence.trim());
}


const lipsum = path.resolve(__dirname, './lipsum.txt');
const lipsumToUpper = path.resolve(__dirname, './lipsumToUpper.txt');
const lipsumToLower = path.resolve(__dirname, './lipsumToLower.txt');
// const lipsumSorted = path.resolve(__dirname, './lipsumSorted.txt');
const lipsumSentence = path.resolve(__dirname, './lipsumSentence.txt')
const filenames = path.resolve(__dirname, './filenames.txt');


function problem2() {
    readFile(lipsum, () => {
        readFileUpper(lipsum, () => {
            writeFile(lipsumToUpper, ()=>{
                appendFile(filenames, ()=> {
                    readFile(lipsumToUpper, () => {
                        readFileLower(lipsumToUpper, () => {
                            appendFile(lipsumToLower, () => {
                                splitBySentence(lipsumToLower, () => {
                                    appendFile(filenames, () => {
                                        readFile(filenames, () => {
                                            deleteFile(filenames, () => {
                                                console.log('Deleting all the files!')
                                            })
                                        })
                                    })
                                })
                            })
                        })
                    })
                })
            })
        })
    })

}

module.exports = problem2

