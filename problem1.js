const fs = require('fs');

const path = require('path');

function makeDirectory(directoryName, callback) {
    // body...
    fs.mkdir(directoryName, function (err) {
        // body...
        if(err){
            console.log(err);
        }
        else{
            console.log('created directory');
            callback();
        }
    });
}



function createFile(filePath, data, callback) {
    // body...
    fs.writeFile(filePath, data, 'utf-8', function (data, err){
        if(err){
            console.log(err);
        }
        else{
            console.log('created file');
            callback();
        }
    });
}


// function deleteFile(filePath) {

// }

function deleteFile(filePath) {
    let pathName = path.join(__dirname, filePath);
    let files = fs.readdirSync(pathName);
    let deleteFiles = files.map((eachfile) => {
        let filePath = path.join(pathName, eachfile);
        fs.unlink(filePath, function (err) {
            if (err) {
                console.log(err);
            } else {
                console.log("Deleted files");
            }
        });
    });
}


function problem1() {
    // body...
    // console.log('heloo');
    const directoryName = path.join(__dirname, 'temp1/');
    makeDirectory(directoryName, () => {
        createFile(`${directoryName} + file1.json`, 'hello', () =>{
            createFile(`${directoryName} + file2.json`, 'hi', () => {
                deleteFile('temp1')
            });
        });
    });

}

module.exports = problem1
